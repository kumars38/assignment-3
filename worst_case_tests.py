from sorts import dual_pivot_copy, tri_pivot_copy, quad_pivot_copy
from lab3 import my_quicksort, quicksort_copy, create_random_list, create_near_sorted_list
from xlsxwriter import Workbook
import timeit
import random

workbook = Workbook('worst-case-data.xlsx')
worksheet = workbook.add_worksheet()
def insertionSort(arr): 
    #taken from geeksforgeeks
    # Traverse through 1 to len(arr) 
    for i in range(1, len(arr)): 
  
        key = arr[i] 
  
        # Move elements of arr[0..i-1], that are 
        # greater than key, to one position ahead 
        # of their current position 
        j = i-1
        while j >= 0 and key < arr[j] : 
                arr[j + 1] = arr[j] 
                j -= 1
        arr[j + 1] = key 

def bubbleSort(arr): 
    n = len(arr) 
   #taken from geeksforgeeks
    # Traverse through all array elements 
    for i in range(n-1): 
    # range(n) also work but outer loop will repeat one time more than needed. 
  
        # Last i elements are already in place 
        for j in range(0, n-i-1): 
  
            # traverse the array from 0 to n-i-1 
            # Swap if the element found is greater 
            # than the next element 
            if arr[j] > arr[j+1] : 
                arr[j], arr[j+1] = arr[j+1], arr[j] 

def worst_test():
    xa = []
    xw = []
    ta=[]
    tw=[]

    for i in range(100,1100,10):
        time_sum=0
        for j in range(100):
            xw = create_random_list(i)
            xw = sorted(xw)
            t1= timeit.default_timer()
            quad_pivot_copy(xw)
            t2=timeit.default_timer()
            time_sum += (t2-t1)
        tw.append(time_sum/10)
        """ time_sum=0
        for j in range(10):
            xa = create_random_list(i)
            t1= timeit.default_timer()
            dual_pivot_copy(xa)
            t2=timeit.default_timer()
            time_sum += (t2-t1)
        ta.append(time_sum/10) """

    n=0
    for i in range(1,100):
        worksheet.write(i, 0, 100+n)
        #worksheet.write(i, 1, ta[i-1])
        worksheet.write(i, 1, tw[i-1])
        n=n+10
    workbook.close()
worst_test()
def near_sorted_tests():
    worksheet2 = workbook.add_worksheet()
    tq=[]
    ti=[]
    tb=[]
    for i in range(1,11):
        n = i/10
        time_sum_quick=0
        time_sum_insert=0
        time_sum_bubble=0
        for j in range(100):
            L_quick = create_near_sorted_list(1000,n)
            L_insert = L_quick.copy()
            L_bubble = L_quick.copy()
            t1q = timeit.default_timer()
            quad_pivot_copy(L_quick)
            t2q = timeit.default_timer()
            time_sum_quick+=(t2q-t1q)

            t1i = timeit.default_timer()
            insertionSort(L_insert)
            t2i = timeit.default_timer()
            time_sum_insert+=(t2i-t1i)

            t1b = timeit.default_timer()
            bubbleSort(L_bubble)
            t2b = timeit.default_timer()
            time_sum_bubble+=(t2b-t1b)
        tq.append(time_sum_quick/100)
        ti.append(time_sum_insert/100)
        tb.append(time_sum_bubble/100)
    for i in range(1,11):
        worksheet2.write(i, 0, i/10)
        worksheet2.write(i, 1, tq[i-1])
        worksheet2.write(i, 2, ti[i-1])
        worksheet2.write(i, 3, tb[i-1])
    workbook.close()
#near_sorted_tests()


