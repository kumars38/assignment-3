import random

## In-Place Quicksort

def quicksort_inplace(L):
    # no changes needed if singleton list
    if len(L) < 2:
        return
    return partition(L, 0, len(L)-1)

def partition(L, l, r):
    # recursive base case: length of sublist is 0 or 1
    if l >= r:
        return
    
    # keep track of start and end index of sublist
    start = l
    end = r
    # assign pivot to first element of sublist
    pivot = L[start]
    # keep checking pointers until they cross
    while l <= r:
        while L[l] < pivot:
            l += 1
        while L[r] > pivot:
            r -= 1
        # check that pointers haven't crossed
        if l <= r:
            # swap elements
            L[l],L[r] = L[r],L[l]
            l += 1
            r -= 1
    # quicksort the partitioned sublists based on final l, r positions
    partition(L, start, r)
    partition(L, l, end)

## Multi-Pivot Quicksorts

def dual_pivot_quicksort(L): 
    copy = dual_pivot_copy(L)
    for i in range(len(L)):
        L[i]=copy[i]

def dual_pivot_copy(L):
    if(len(L)<2):
        return L
    lPivot=L[0]
    rPivot=L[-1]
    left,right,middle=[],[],[]
    if(rPivot<lPivot):
        lPivot=L[-1]
        rPivot=L[0]
    for num in L[1:len(L)-1]:
        if(num<lPivot):
            left.append(num)
        elif(num<rPivot):
            middle.append(num)
        else:
            right.append(num)
    return dual_pivot_copy(left)+[lPivot]+dual_pivot_copy(middle)+[rPivot]+dual_pivot_copy(right)

def tri_pivot_quicksort(L): 
    copy = tri_pivot_copy(L)
    for i in range(len(L)):
        L[i]=copy[i]

def tri_pivot_copy(L):
    if(len(L)<2):
        return L
    if(len(L)<3):
        return [min(L[0],L[1]),max(L[0],L[1])]


    a1 = min(L[0], L[1], L[2])
    a3 = max(L[0], L[1], L[2])
    a2 = (L[0]+ L[1] + L[2]) - a1 - a3

    lPivot=a1
    mPivot=a2
    rPivot=a3

    left,right,middleR,middleL=[],[],[],[]
    
    for num in L[3:]:
        if(num<lPivot):
           left.append(num)
        elif(num<mPivot):
            middleL.append(num)
        elif(num<rPivot):
            middleR.append(num)
        else:
            right.append(num)
    
    return tri_pivot_copy(left)+[lPivot]+tri_pivot_copy(middleL)+[mPivot]+tri_pivot_copy(middleR)+[rPivot]+tri_pivot_copy(right)

def quad_pivot_quicksort(L):
    copy = quad_pivot_copy(L)
    for i in range(len(L)):
        L[i] = copy[i]

def quad_pivot_copy(L):
    # base cases
    if len(L) < 2:
        return L
    if len(L) < 3:
        return [min(L[0],L[1]),max(L[0],L[1])]
    if len(L) < 4:
        return sorted(L)

    # len > 4, quad pivot quicksort
    # select the pivots
    f = sorted(L[0:4])
    piv1 = f[0]
    piv2 = f[1]
    piv3 = f[2]
    piv4 = f[3]
    list1, list2, list3, list4, list5 = [],[],[],[],[]
    for num in L[4:]:
        if num < piv1:
            list1.append(num)
        elif num < piv2:
            list2.append(num)
        elif num < piv3:
            list3.append(num)
        elif num < piv4:
            list4.append(num)
        else:
            list5.append(num)
    return quad_pivot_copy(list1)+[piv1]+quad_pivot_copy(list2)+[piv2]+quad_pivot_copy(list3)+[piv3]+quad_pivot_copy(list4)+[piv4]+quad_pivot_copy(list5)

def final_sort(L):
    copy = final_sort_copy(L)
    for i in range(len(L)):
        L[i] = copy[i]

def final_sort_copy(L):
    if len(L) < 2:
        return L
    elif len(L) < 3:
        return [min(L[0],L[1]),max(L[0],L[1])]
    elif len(L) < 4:
        return sorted(L)
    elif len(L) < 10:
        for i in range(1, len(L)): 
            key = L[i] 
            j = i-1
            while j >= 0 and key < L[j] : 
                L[j + 1] = L[j] 
                j -= 1
            L[j + 1] = key
        return L
    else:
        f = sorted(L[0:4])
        piv1 = f[0]
        piv2 = f[1]
        piv3 = f[2]
        piv4 = f[3]
        list1, list2, list3, list4, list5 = [],[],[],[],[]
        for num in L[4:]:
            if num < piv1:
                list1.append(num)
            elif num < piv2:
                list2.append(num)
            elif num < piv3:
                list3.append(num)
            elif num < piv4:
                list4.append(num)
            else:
                list5.append(num)
        return final_sort_copy(list1)+[piv1]+final_sort_copy(list2)+[piv2]+final_sort_copy(list3)+[piv3]+final_sort_copy(list4)+[piv4]+final_sort_copy(list5)

## For Testing
def create_random_list(n, m):
    L = []
    for _ in range(n):
        L.append(random.randint(-m,m))
    return L



'''L = create_random_list(50, 100)
print("Unsort: ",L)
#quicksort_inplace(L)
#dual_pivot_quicksort(L)
#tri_pivot_quicksort(L)
#quad_pivot_quicksort(L)
#final_sort(L)
print("Sorted: ",L)'''




