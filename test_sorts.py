from sorts import quicksort_inplace, dual_pivot_copy, tri_pivot_copy, quad_pivot_copy
from lab3 import quicksort_copy, create_random_list
from xlsxwriter import Workbook
import timeit
import random

# Timing Tests for Traditional vs. In-Place Quicksort

workbook3 = Workbook('inplace-quicksort-data.xlsx')
worksheet3 = workbook3.add_worksheet()

t1, t2 = [], []
N = 2000
T = 10
L = []

# lists from length 1 to N
for i in range(N):
    L.append(random.randint(-10000,10000))
    # T trials at each length
    for j in range(T):
        # shallow copy
        L1 = [e for e in L]

        # Timing Tests
        # Traditional Quicksort from lab3.py
        start = timeit.default_timer()
        quicksort_copy(L1)
        end = timeit.default_timer()
        if j == 0:
            t1.append(end-start)
        else:
            t1[i] += end-start
  
        # In-Place Quicksort
        start = timeit.default_timer()
        quicksort_inplace(L1)
        end = timeit.default_timer()
        if j == 0:
            t2.append(end-start)
        else:
            t2[i] += end-start

# Average trials
for i in range(N):
    t1[i] /= T
    t2[i] /= T

# After tests, write to excel file
for i in range(N):
    worksheet3.write(i, 0, i+1)
    worksheet3.write(i, 1, t1[i])
    worksheet3.write(i, 2, t2[i])
workbook3.close()

# Timing Tests for N-Pivot Quicksorts (N = 1,2,3,4)

workbook = Workbook('multi-pivot-data.xlsx')
worksheet = workbook.add_worksheet()

t1, t2, t3, t4 = [], [], [], []
N = 2000
T = 10
L = []

# lists from length 1 to N
for i in range(N):
    L.append(random.randint(-10000,10000))
    # T trials at each length
    for j in range(T):
        # Timing Tests
        # Single-Pivot
        start = timeit.default_timer()
        quicksort_copy(L)
        end = timeit.default_timer()
        if j == 0:
            t1.append(end-start)
        else:
            t1[i] += end-start
  
        # Dual-Pivot
        start = timeit.default_timer()
        dual_pivot_copy(L)
        end = timeit.default_timer()
        if j == 0:
            t2.append(end-start)
        else:
            t2[i] += end-start

        # Tri-Pivot
        start = timeit.default_timer()
        tri_pivot_copy(L)
        end = timeit.default_timer()
        if j == 0:
            t3.append(end-start)
        else:
            t3[i] += end-start

        # Quad-Pivot
        start = timeit.default_timer()
        quad_pivot_copy(L)
        end = timeit.default_timer()
        if j == 0:
            t4.append(end-start)
        else:
            t4[i] += end-start

# Average trials
for i in range(N):
    t1[i] /= T
    t2[i] /= T
    t3[i] /= T
    t4[i] /= T

# After tests, write to excel file
for i in range(N):
    worksheet.write(i, 0, i+1)
    worksheet.write(i, 1, t1[i])
    worksheet.write(i, 2, t2[i])
    worksheet.write(i, 3, t3[i])
    worksheet.write(i, 4, t4[i])
workbook.close()

## Elementary Sorts

def insertion(L):
    for i in range(1, len(L)): 
        key = L[i] 
        j = i-1
        while j >= 0 and key < L[j] : 
            L[j + 1] = L[j] 
            j -= 1
        L[j + 1] = key
    return L

def selection_sort(L):
    for i in range(len(L)): 
      
        min_idx = i 
        for j in range(i+1, len(L)): 
            if L[min_idx] > L[j]: 
                min_idx = j 
        L[i], L[min_idx] = L[min_idx], L[i] 
    return L

def bubbleSort(L):
    for passnum in range(len(L)-1,0,-1):
        for i in range(passnum):
            if L[i]>L[i+1]:
                temp = L[i]
                L[i] = L[i+1]
                L[i+1] = temp
    return L

# Timing Tests for Small Lists

workbook2 = Workbook('short-lists-data.xlsx')
worksheet2 = workbook2.add_worksheet()

time1,time2,time3,time4 = [],[],[],[]
L,L1,L2,L3,L4=[],[],[],[],[]
N=10
T=100000
for i in range(N):
    L.append(random.randint(-10000,10000))
    for j in range(T):
        L1,L2,L3,L4=[],[],[],[]
        for k in range(i):
            L1.append(L[k])
            L2.append(L[k])
            L3.append(L[k])
            L4.append(L[k])
        
        start = timeit.default_timer()
        quad_pivot_copy(L1)
        end = timeit.default_timer()
        if j==0:
            time1.append(end-start)
        else:
            time1[i]+=end-start
        
        start = timeit.default_timer()
        insertion(L2)
        end = timeit.default_timer()
        if j==0:
            time2.append(end-start)
        else:
            time2[i]+=end-start

        start = timeit.default_timer()
        bubbleSort(L3)
        end = timeit.default_timer()
        if j==0:
            time3.append(end-start)
        else:
            time3[i]+=end-start

        start = timeit.default_timer()
        selection_sort(L4)
        end = timeit.default_timer()
        if j==0:
            time4.append(end-start)
        else:
            time4[i]+=end-start

# Average trials
for i in range(N):
    time1[i] /= T
    time2[i] /= T
    time3[i] /= T
    time4[i] /= T

# After tests, write to excel file
for i in range(N):
    worksheet2.write(i, 0, i+1)
    worksheet2.write(i, 1, time1[i])
    worksheet2.write(i, 2, time2[i])
    worksheet2.write(i, 3, time3[i])
    worksheet2.write(i, 4, time4[i])
workbook2.close()