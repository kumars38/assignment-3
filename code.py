from sorts import quicksort_inplace, dual_pivot_copy, tri_pivot_copy, quad_pivot_copy
from lab3 import quicksort_copy, create_random_list, create_near_sorted_list
from xlsxwriter import Workbook
import timeit
import random

# Timing Tests for Traditional vs. In-Place Quicksort

workbook3 = Workbook('inplace-quicksort-data.xlsx')
worksheet3 = workbook3.add_worksheet()

t1, t2 = [], []
N = 2000
T = 10
L = []

# lists from length 1 to N
for i in range(N):
    L.append(random.randint(-10000,10000))
    # T trials at each length
    for j in range(T):
        # shallow copy
        L1 = [e for e in L]

        # Timing Tests
        # Traditional Quicksort from lab3.py
        start = timeit.default_timer()
        quicksort_copy(L1)
        end = timeit.default_timer()
        if j == 0:
            t1.append(end-start)
        else:
            t1[i] += end-start
  
        # In-Place Quicksort
        start = timeit.default_timer()
        quicksort_inplace(L1)
        end = timeit.default_timer()
        if j == 0:
            t2.append(end-start)
        else:
            t2[i] += end-start

# Average trials
for i in range(N):
    t1[i] /= T
    t2[i] /= T

# After tests, write to excel file
for i in range(N):
    worksheet3.write(i, 0, i+1)
    worksheet3.write(i, 1, t1[i])
    worksheet3.write(i, 2, t2[i])
workbook3.close()

# Timing Tests for N-Pivot Quicksorts (N = 1,2,3,4)

workbook = Workbook('multi-pivot-data.xlsx')
worksheet = workbook.add_worksheet()

t1, t2, t3, t4 = [], [], [], []
N = 2000
T = 10
L = []

# lists from length 1 to N
for i in range(N):
    L.append(random.randint(-10000,10000))
    # T trials at each length
    for j in range(T):
        # Timing Tests
        # Single-Pivot
        start = timeit.default_timer()
        quicksort_copy(L)
        end = timeit.default_timer()
        if j == 0:
            t1.append(end-start)
        else:
            t1[i] += end-start
  
        # Dual-Pivot
        start = timeit.default_timer()
        dual_pivot_copy(L)
        end = timeit.default_timer()
        if j == 0:
            t2.append(end-start)
        else:
            t2[i] += end-start

        # Tri-Pivot
        start = timeit.default_timer()
        tri_pivot_copy(L)
        end = timeit.default_timer()
        if j == 0:
            t3.append(end-start)
        else:
            t3[i] += end-start

        # Quad-Pivot
        start = timeit.default_timer()
        quad_pivot_copy(L)
        end = timeit.default_timer()
        if j == 0:
            t4.append(end-start)
        else:
            t4[i] += end-start

# Average trials
for i in range(N):
    t1[i] /= T
    t2[i] /= T
    t3[i] /= T
    t4[i] /= T

# After tests, write to excel file
for i in range(N):
    worksheet.write(i, 0, i+1)
    worksheet.write(i, 1, t1[i])
    worksheet.write(i, 2, t2[i])
    worksheet.write(i, 3, t3[i])
    worksheet.write(i, 4, t4[i])
workbook.close()

## Elementary Sorts

def insertion(L):
    for i in range(1, len(L)): 
        key = L[i] 
        j = i-1
        while j >= 0 and key < L[j] : 
            L[j + 1] = L[j] 
            j -= 1
        L[j + 1] = key
    return L

def selection_sort(L):
    for i in range(len(L)): 
      
        min_idx = i 
        for j in range(i+1, len(L)): 
            if L[min_idx] > L[j]: 
                min_idx = j 
        L[i], L[min_idx] = L[min_idx], L[i] 
    return L

def bubbleSort(L):
    for passnum in range(len(L)-1,0,-1):
        for i in range(passnum):
            if L[i]>L[i+1]:
                temp = L[i]
                L[i] = L[i+1]
                L[i+1] = temp
    return L

# Timing Tests for Small Lists

workbook2 = Workbook('short-lists-data.xlsx')
worksheet2 = workbook2.add_worksheet()

time1,time2,time3,time4 = [],[],[],[]
L,L1,L2,L3,L4=[],[],[],[],[]
N=10
T=100000
for i in range(N):
    L.append(random.randint(-10000,10000))
    for j in range(T):
        L1,L2,L3,L4=[],[],[],[]
        for k in range(i):
            L1.append(L[k])
            L2.append(L[k])
            L3.append(L[k])
            L4.append(L[k])
        
        start = timeit.default_timer()
        quad_pivot_copy(L1)
        end = timeit.default_timer()
        if j==0:
            time1.append(end-start)
        else:
            time1[i]+=end-start
        
        start = timeit.default_timer()
        insertion(L2)
        end = timeit.default_timer()
        if j==0:
            time2.append(end-start)
        else:
            time2[i]+=end-start

        start = timeit.default_timer()
        bubbleSort(L3)
        end = timeit.default_timer()
        if j==0:
            time3.append(end-start)
        else:
            time3[i]+=end-start

        start = timeit.default_timer()
        selection_sort(L4)
        end = timeit.default_timer()
        if j==0:
            time4.append(end-start)
        else:
            time4[i]+=end-start

# Average trials
for i in range(N):
    time1[i] /= T
    time2[i] /= T
    time3[i] /= T
    time4[i] /= T

# After tests, write to excel file
for i in range(N):
    worksheet2.write(i, 0, i+1)
    worksheet2.write(i, 1, time1[i])
    worksheet2.write(i, 2, time2[i])
    worksheet2.write(i, 3, time3[i])
    worksheet2.write(i, 4, time4[i])
workbook2.close()

workbook = Workbook('worst-case-data.xlsx')
worksheet = workbook.add_worksheet()
def insertionSort(arr): 
    #taken from geeksforgeeks
    # Traverse through 1 to len(arr) 
    for i in range(1, len(arr)): 
  
        key = arr[i] 
  
        # Move elements of arr[0..i-1], that are 
        # greater than key, to one position ahead 
        # of their current position 
        j = i-1
        while j >= 0 and key < arr[j] : 
                arr[j + 1] = arr[j] 
                j -= 1
        arr[j + 1] = key 

def bubbleSort(arr): 
    n = len(arr) 
   #taken from geeksforgeeks
    # Traverse through all array elements 
    for i in range(n-1): 
    # range(n) also work but outer loop will repeat one time more than needed. 
  
        # Last i elements are already in place 
        for j in range(0, n-i-1): 
  
            # traverse the array from 0 to n-i-1 
            # Swap if the element found is greater 
            # than the next element 
            if arr[j] > arr[j+1] : 
                arr[j], arr[j+1] = arr[j+1], arr[j] 

def worst_test():
    xa = []
    xw = []
    ta=[]
    tw=[]

    for i in range(100,1100,10):
        time_sum=0
        for j in range(100):
            xw = create_random_list(i)
            xw = sorted(xw)
            t1= timeit.default_timer()
            quad_pivot_copy(xw)
            t2=timeit.default_timer()
            time_sum += (t2-t1)
        tw.append(time_sum/10)
        """ time_sum=0
        for j in range(10):
            xa = create_random_list(i)
            t1= timeit.default_timer()
            dual_pivot_copy(xa)
            t2=timeit.default_timer()
            time_sum += (t2-t1)
        ta.append(time_sum/10) """

    n=0
    for i in range(1,100):
        worksheet.write(i, 0, 100+n)
        #worksheet.write(i, 1, ta[i-1])
        worksheet.write(i, 1, tw[i-1])
        n=n+10
    workbook.close()
worst_test()
def near_sorted_tests():
    worksheet2 = workbook.add_worksheet()
    tq=[]
    ti=[]
    tb=[]
    for i in range(1,11):
        n = i/10
        time_sum_quick=0
        time_sum_insert=0
        time_sum_bubble=0
        for j in range(100):
            L_quick = create_near_sorted_list(1000,n)
            L_insert = L_quick.copy()
            L_bubble = L_quick.copy()
            t1q = timeit.default_timer()
            quad_pivot_copy(L_quick)
            t2q = timeit.default_timer()
            time_sum_quick+=(t2q-t1q)

            t1i = timeit.default_timer()
            insertionSort(L_insert)
            t2i = timeit.default_timer()
            time_sum_insert+=(t2i-t1i)

            t1b = timeit.default_timer()
            bubbleSort(L_bubble)
            t2b = timeit.default_timer()
            time_sum_bubble+=(t2b-t1b)
        tq.append(time_sum_quick/100)
        ti.append(time_sum_insert/100)
        tb.append(time_sum_bubble/100)
    for i in range(1,11):
        worksheet2.write(i, 0, i/10)
        worksheet2.write(i, 1, tq[i-1])
        worksheet2.write(i, 2, ti[i-1])
        worksheet2.write(i, 3, tb[i-1])
    workbook.close()
#near_sorted_tests()